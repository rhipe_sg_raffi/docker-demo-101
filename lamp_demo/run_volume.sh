#!/bin/sh
./stop.sh
./rm.sh

if [ ! -d "./example.com/log" ]; then
    mkdir -p ./example.com/log
fi
docker run -p 80:80 -d --name "lamp_demo" -v "`pwd`/example.com":/var/www/example.com -ti rhiperaffi/oss_demo_lamp
