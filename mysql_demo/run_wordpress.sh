#!/bin/bash
docker pull docker.io/rhiperaffi/oss_demo_wordpress

docker stop wp_mysql_demo 2> /dev/null
docker rm wp_mysql_demo 2> /dev/null

docker run -p 80:80 -dti --net net1 \
                    --name wp_mysql_demo \
                    rhiperaffi/oss_demo_wordpress

