#!/bin/bash
./stop.sh 2> /dev/null
./rm.sh 2> /dev/null
docker run -p 80:80 -dti --name wordpress_demo \
                    -v wp_mysql:/var/lib/mysql \
                    -v wordpress:/var/www/example.com/public_html/wordpress \
                    rhiperaffi/oss_demo_wordpress
